using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneAliada : MonoBehaviour
{
    private const float yDie = -30.0f;
    public GameObject explosion;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < yDie)
        {
            Destroy(gameObject);
        }
    }
    private void OnMouseDown()
    {
        Destroy(Instantiate(explosion, transform.position, Quaternion.identity), 2.0f);
        GameManager.live--;
        GameManager.currentNumberDestroyeStones -= 2; 
        Destroy(gameObject);

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            print("Collision");
            GameManager.live--;
            Destroy(gameObject);
        }
    }
}
